// Express Setup

const express = require('express');

// Mongoose is a package that allows creation of schemas to model our data structures
const mongoose = require('mongoose');

const app = express();
const port = 3000;

// Mongoose Connection
// Mongoose uses the connect function to connect to the cluster in the mongoDb Atlas
/* It takes 2 arguments:
    1. Connection string from mongoDb Atlas
    2. Object that contains the middlewares or standard that mongoDb uses.
*/

mongoose.connect(`mongodb+srv://admin1234:admin1234@zuitt-batch197.fk6bhtr.mongodb.net/S35-Activity?retryWrites=true&w=majority`,{
    useNewUrlParser: true, //it allows us to avoid any current and or future errors while connecting to mongoDb
    useUnifiedTopology: true //false by default.Set to true to opt in to using the mongoDb driver's new connection management engine
})

// Initializes the mongoose connection to the mongoDb database by assigning mongoose.connection to the db variable
let db = mongoose.connection;

/* Listen to the events of the connection by using the 'on()' function of the mongoose connection and logs the details in the console based of the event (error or successful)
*/
db.on('error', console.error.bind(console,"Connection"));
db.once('open',() => console.log('Connection to MongoDb!'));



// Creating a schema
const taskSchema = new mongoose.Schema({
    name: String,
    status: {
        type: String,
        default: 'Pending'
    }
})

const userSchema = new mongoose.Schema({
    username: String,
    password: String
})

// MODEL
/* The variable/object Task can now be used to run commands for interacting with our db. Task is capitalized following the MVC approach for the naming convention.
Models must be in singular form and capitalized. THe first parameter is used to specify the Name of the collection where we will store the data.
THe second parameter is used to specify the schema/blueprint of the documents that will be stored in pur mongodb collection
*/
const Task = mongoose.model('Task',taskSchema);

const User = mongoose.model('User', userSchema);


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.post('/tasks',(req, res) => {
    // business logic
    Task.findOne({name: req.body.name}, (error, result) => {
        if(error){
            return res.send(error)
        }else if (result != null && result.name == req.body.name){
            return res.send("Duplicate task found")
        }else{
            let newTask = new Task({
                name: req.body.name
            })

            newTask.save((error, savedTask) => {
                if(error){
                    return console.error(error)
                }else{
                    return res.status(201).send('New Task Created')
                }
            })
        }
    })


})

// Get all tasks 

app.get('/tasks',(req,res) => {
    Task.find({},(error,result) => {
        if(error){
            return res.send(error)
        }else {
            return res.status(200).json({tasks: result})
        }
    })
})



// USERS ROUTE

app.post('/signup',(req,res) => {

    User.findOne({username: req.body.username}, (error, result) => {
        if(error){
            return res.send(error)
        }else if (result != null && result.username == req.body.username){
            return res.send(`User ${result.username} already exist`);
        }else{
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            })

            newUser.save((error, savedUser) => {
                if(error){
                    return console.error(error)
                }else{
                    return res.status(201).send({
                        message: 'New User Created',
                        user: savedUser
                    })
                }
            })
        }
    })


})


app.listen(port, () => console.log(`Server is running at port:${port}`))